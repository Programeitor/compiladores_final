/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   semantic.h
 * Author: ulises
 *
 * Created on 5 de junio de 2017, 11:49
 */

#ifndef SEMANTIC_H
#define SEMANTIC_H

#ifdef __cplusplus
extern "C" {
#endif
    
#include <stdio.h>
#include <string.h>
#include "symbol_table.h"
#include "type_table.h"
    
    struct _dir_stack{
        int dir[20];
        int count;
    };

    typedef struct _dir_stack  dir_stack;
 

    dir_stack dirStack;
    int index_temp;
    int index_count;
    int index_label;

    //simbolo que va entrar a la tabla de simbolos
    symbol ctr_symbol;
    
    
    void newTemp(char *dir);
    void newLabel(char *dir);
    void newIndex(char *dir);
    void init_index();
    


#ifdef __cplusplus
}
#endif

#endif /* SEMANTIC_H */

