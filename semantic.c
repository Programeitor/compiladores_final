/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
#include "semantic.h"

void newTemp(char *dir){
    char temp[20];
    strcpy(temp , "_t");
    char num[18];
    sprintf(num, "%d", index_label);	
    index_label++;
    strcat(temp,num);
    strcpy(dir, temp);
}


void newLabel(char *dir){
    char temp[20];
    strcpy(temp , "_L");
    char num[18];
    sprintf(num, "%d", index_temp);	
    index_temp++;
    strcat(temp,num);
    strcpy(dir, temp);
}


void newIndex(char *dir){
    char temp[20];
    strcpy(temp , "_I");
    char num[18];
    sprintf(num, "%d", index_count);	
    index_count++;
    strcat(temp,num);
    strcpy(dir, temp);
}


void init_index(){
    index_temp = 0;
    index_count = 0;
    index_label = 0;
    dirStack.count = 0;        
}