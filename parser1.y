%{
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "symbol_table.h"
#include "type_table.h"
#include "semantic.h"
#include "intermediate_code.h"

extern FILE* yyin;
extern int yylex();
extern FILE* yyout;
void yyerror(char* s);

// variables que sustituye en atributo tipo
// de el no terminal list
int w=0, t=0;

// globales para B
int w_b, t_b;

int dir = 0;

int tipo_func;

lcode *gcode;

void init();
void amp(char *dir, int t1, int t2, char* res);
int max(int t1, int t2);
void insert(char *id, int dir, int tipo, int sym, int args[], int num);

%} 


%union{
    struct{
        int ival;
        double dval;
        //char *sval;
        char sval[20];
        int tipo;
    } num;
    
    struct{
        char dir[20];
        int tipo;
        lcode *code;
    }exp;
    
    struct{
        int tipo;
        int dim;        
    } tipo;

    struct{         
        int tipo;        
        char next[20];
        char _false[20];
        lcode *code;
    } sent;
    
    struct{
        int args[30];
        int count;
    } arg_list;

    struct{
        char _true[20];
        char _false[20];   
        lcode *code;
    } sent_bool;
    
    int line;   
    
    char sval[20];
}


%token<sval> ID
%token<num> NUM;
%token COMA PYC
%token VOID CHAR INT FLOAT DOUBLE 
%token STRUCT DEFINE RETURN
%token IF WHILE DO FOR
%token LKEY RKEY

/* precedencia de operadores*/
%left ASIG //=
%left OR   // ||
%left AND  // &&
%left<sval> EQ DIF // == !=
%left<sval> GT LT GET LET // > < >= <=
%left MENOS //-
%left MAS   //+
%left MOD   //%
%left DIV   // /
%left MUL   // *
%right NOT  // !
         /* (   )    [    ]     . */
%nonassoc LPAR RPAR LCOR RCOR PUNTO
%nonassoc IFX  //para eleminar la ambiguedad
%nonassoc ELSE 

/*Declaración del tipo de los no terminales*/
%type<tipo> type c_array base
//No se incluye a list porque
//no va a tener los atributos heredados
%type<exp> expresion
%type<arg_list> args args_list
%type<sent> sents sent sent_else
%type<sent_bool> sent_bool
%type<sval> op_rel

%start program


%%
/*P -> D F */
program : {    
    //printf("iniciando a gcode2\n");
}
decl funcs { 
    print_symbols(0, "global");
    print_ci(gcode);
    print_code( gcode);
    empty_code(gcode);
    } ;

/* D -> D T L ; | null */
decl: decl type { 
        t = $2.tipo;
        w = $2.dim;
    } list PYC{
    //printf("D-> D T L;\n");
    } 
    
    |
    { 
    //printf("D->3\n");
    };

/* T -> B C */
type: base{
	t_b = $1.tipo;
	w_b = $1.dim;
        
     } c_array{ 
	$$.tipo = $3.tipo;
	$$.dim = $3.dim;
        //printf("T -> B C\n");
     };

/* B-> void | char | int | float | double */
base: VOID{        
        $$.tipo = 0;
        $$.dim = 1;
        //printf("B->void\n");
        } 
        | CHAR{            
            $$.tipo = 1;
            $$.dim = 2;
            //printf("B->char\n");
        }
        | INT{        
        $$.tipo = 2;
        $$.dim = 4;
        //printf("B->int\n");
        } 
        | FLOAT{            
            $$.tipo = 3;
            $$.dim = 8;
            //printf("B->float\n");
        }
        | DOUBLE{
            $$.tipo = 4;
            $$.dim = 16;
            //printf("B->double\n");
        }
        ;

/* C -> [ num ] C1 | null */
c_array : LCOR NUM RCOR c_array {
        type t;
        if($2.tipo ==2){
            strcpy(t.tipo, "array");
            t.dim = $2.ival;
            t.base = $4.tipo;
            $$.tipo = insert_type(t);
            $$.dim = $4.dim;
            //printf("C -> [ num ] C1\n");
        }else{
            yyerror("La dimensión debe de ser un valor entero");
        }
    }

	| {	
		$$.tipo = t_b;
		$$.dim = w_b;
                //printf("C -> null\n");
	};

/* L -> L1 , id | id */
list : list COMA ID {
        //printf("L->L,id\n");
        insert($3, dir, t, 0, NULL, -1);
        dir = dir + w;
        //printf("L->L1, id\n");
        }
        | ID{
            //printf("L->id\n");
            insert($1, dir, t, 0, NULL, -1);
            dir = dir + w;
            //printf("L->id\n");
        };
        
/* F -> define T id ( A ) { D Y } F1 | null */        
funcs : DEFINE type ID LPAR{
        //init_code($$);
        tipo_func = $2.tipo;
        create_symbol_table();
        create_type_table();
        dirStack.count++;
        dirStack.dir[dirStack.count] = dir;
        dir = 0;        
    }args_list RPAR LKEY sents RKEY {        
        print_symbols(symStack.sp,$3);
        delete_symbol_table();
        delete_type_table();
        dir = dirStack.dir[dirStack.count];
        dirStack.count--;    
        insert($3, -1 , tipo_func, 1, $6.args, $6.count);               
        insert_code(gcode, create_ci("label", NULL, NULL, $3));
        codecat(gcode, $9.code);
        print_ci(gcode); 
             
    }funcs 
    {
        //printf("F -> define T id ( A ) { D Y } F1\n");
    }
    |{
        //printf("F -> null\n");
    } ;
    



/* A -> E | void */
args_list: args {
        $$ = $1;
    //printf("A->E\n");
    } 
    | VOID {
        $$.count = -1;
       //printf("A-> void\n");
    }
    ;

/* E-> E, T id | T id */
args: args COMA type ID {
       insert($4, dir, t, 2, NULL, -1); 
       dir = dir + w;
       $1.count++;
       $1.args[$1.count] = $3.tipo;
       $$ = $1;
    }
    | type ID{    
       insert($2, dir, t, 2, NULL, -1); 
       dir = dir + w;
       $$.count= 0;
       $$.args[$$.count] = $1.tipo;
       
    };

/*  Z -> Z1 X | X */
sents: sents sent {
        printf("S->S I\n");
        $$.code = (lcode*) malloc(sizeof(lcode));
        init_code($$.code);
        codecat($$.code, $1.code);
        codecat($$.code, $2.code);
        
    }
    | sent{
        $$.code = (lcode*) malloc(sizeof(lcode));
        init_code($$.code);
        codecat($$.code, $1.code);
        printf("S->I\n");
        
    };
    
/* X -> id = G; */
sent:  ID ASIG expresion PYC{   
    if(search_symbol($1, symStack.sp) == 1){
        if(get_tipo($1, symStack.sp)  == $3.tipo){
            newLabel($$.next);
            $$.code = (lcode*) malloc(2*sizeof(lcode));
            init_code($$.code);   
            codecat($$.code, $3.code);
            insert_code($$.code, create_ci("=", $3.dir, NULL, $1));
            //print_ci($$.code);
            //printf("codigo asginacipon\n");
        }else{
            yyerror("Error Semántico: los tipos no coincide\n");
        }
    }else if(search_symbol($1, 0) == 1){
        if(get_tipo($1, 0)  == $3.tipo){
            //fprintf(yyout, "%s = %s \n ", $1, $3.dir);                        
            newLabel($$.next);
            $$.code = (lcode*) malloc(2*sizeof(lcode));
            init_code($$.code);
            codecat($$.code, $3.code);
            insert_code($$.code, create_ci("=", $3.dir, NULL, $1));
            //printf("codigo asginacipon\n");
            //print_ci($$.code);
        }else{
            yyerror("Error Semántico: los tipos no coincide\n");
        }
    }else{
        yyerror("Error Semántico: el id no ha sido declarado\n");
    }
    printf("I->id= E;\n");
    }
    |
    WHILE LPAR sent_bool RPAR sent{        
        
        strcpy($$.next, $3._false);
        $$.code = (lcode*) malloc(sizeof(lcode));
        init_code($$.code);   
        insert_code($$.code, create_ci("label", NULL, NULL, $5.next));
        codecat($$.code, $3.code);
        insert_code($$.code, create_ci("label", NULL, NULL, $3._true));
        codecat($$.code, $5.code);
        insert_code($$.code, create_ci("goto", NULL, NULL, $5.next));
        
        //printf("S->while(B)S\n");
        char temp[20], temp2[20];
        newLabel(temp);
        asig($$.code, $5.next, temp);
        newLabel(temp2);
        //printf("%s\n", temp2);
        asig($$.code, $3._true, temp2);
        //print_ci($$.code);
    }
    |    
        IF LPAR sent_bool RPAR sent sent_else{        
        
        strcpy($$.next, $6.next);
        $$.code = (lcode*) malloc(sizeof(lcode));
        init_code($$.code);           
        codecat($$.code, $3.code);
        insert_code($$.code, create_ci("label", NULL, NULL, $3._true));
        codecat($$.code, $5.code);        
        codecat($$.code, $6.code);
        
        
        
        //printf("S->if (B)S1 S'\n");
        asig($$.code, $5.next, $$.next);
        asig($$.code, $3._false, $6._false);
        char temp[20], temp2[20];
        newLabel(temp);
        asig($$.code, $3._true, temp);
        newLabel(temp2);
        //printf("%s\n", temp2);
        asig($$.code, $$.next, temp2);
        //print_ci($$.code);
    }
    |
    RETURN expresion PYC{             
        $$.tipo = $2.tipo;        
        newLabel($$.next);
        $$.code = (lcode*) malloc(sizeof(lcode));
        init_code($$.code);
        codecat($$.code, $2.code);
        insert_code($$.code, create_ci("return", NULL, NULL, $2.dir));
        //print_ci($$.code);
        //newLabel($$.next);
        printf("X->return G;\n");
    }
    
    ;
    
sent_else: ELSE sent {
        newLabel($$.next);
        newLabel($$._false);
        $$.code = (lcode*) malloc(sizeof(lcode));
        init_code($$.code);
        insert_code($$.code, create_ci("goto",NULL, NULL, $$.next));
        insert_code($$.code, create_ci("label",NULL, NULL, $$._false));
        codecat($$.code, $2.code);		
	}
	| %prec IFX {
		newLabel($$.next);
        newLabel($$._false);
        $$.code = (lcode*) malloc(sizeof(lcode));
        init_code($$.code);		
        insert_code($$.code, create_ci("label",NULL, NULL, $$._false));
	};
    
/* G -> G1 + G2 | G1 * G2  |(G) | id | num */
expresion : expresion MAS expresion {
        $$.tipo = max($1.tipo, $3.tipo);                
        char dirAlpha1[20], dirAlpha2[20];   
        amp($1.dir, $1.tipo, $$.tipo, dirAlpha1);
        amp($3.dir, $3.tipo, $$.tipo, dirAlpha2);        
        newTemp($$.dir);
        $$.code = (lcode*) malloc(sizeof(lcode));
        init_code($$.code);    
        codecat($$.code, $1.code);
        codecat($$.code, $3.code);
        insert_code($$.code, create_ci("+",$1.dir, $3.dir, $$.dir));
        //print_ci($$.code);
        //fprintf(yyout, "%s = %s + %s\n", $$.dir, dirAlpha1, dirAlpha2);
        //printf("E->E+E\n");
    }
    | expresion MUL expresion{
        $$.tipo = max($1.tipo, $3.tipo);                
        char dirAlpha1[20], dirAlpha2[20];   
        amp($1.dir, $1.tipo, $$.tipo, dirAlpha1 );
        amp($3.dir, $3.tipo, $$.tipo, dirAlpha2);        
        newTemp($$.dir);
        $$.code = (lcode*) malloc(sizeof(lcode));
        init_code($$.code);    
        codecat($$.code, $1.code);
        codecat($$.code, $3.code);
        insert_code($$.code, create_ci("*",$1.dir, $3.dir, $$.dir));
        //print_ci($$.code);
        //fprintf(yyout, "%s = %s * %s\n", $$.dir, dirAlpha1, dirAlpha2);        
        //printf("E->E*E\n");
    }
    | LPAR expresion RPAR{
        $$ = $2;	
        //printf("E->(E)\n");
    }
    | ID{
        if(search_symbol($1, symStack.sp) == 1){            
            $$.tipo = get_tipo($1, symStack.sp);
            //printf("El tipo de %s es %d\n", $1, $$.tipo);
            $$.code = (lcode*) malloc(sizeof(lcode));
            init_code($$.code);
            strcpy($$.dir, $1);  
            
        }else if(search_symbol($1, 0) == 1){
            $$.tipo = get_tipo($1, 0);
            //printf("El tipo de %s es %d\n", $1, $$.tipo);
            $$.code = (lcode*) malloc(sizeof(lcode));
            init_code($$.code);
            strcpy($$.dir, $1);
            
        }else{
            yyerror("El id no fue declarado\n");
        }   
        //printf("E->id\n");
                
    }
    | NUM{
        $$.tipo = $1.tipo;
        strcpy($$.dir, $1.sval); 
        $$.code = (lcode*) malloc(sizeof(lcode));
        init_code($$.code);
        //printf("E->num\n");
    };


/* W -> W1 || W2 | W1 && W2 | !W1 | true | false | G1 oprel G2*/
sent_bool: sent_bool OR sent_bool {
            strcpy($$._true, $3._true);
            strcpy($$._false, $3._false);
            $$.code = (lcode*) malloc(sizeof(lcode));
            init_code($$.code);
            codecat($$.code, $1.code);
            insert_code($$.code, create_ci("label", NULL, NULL, $1._false));
            codecat($$.code, $3.code);
            char temp[20];
            newLabel(temp);
            asig($$.code, $1._true, $3._true);
            asig($$.code, $3._false, temp);
            
        }
        |
        expresion op_rel expresion{
        newIndex($$._true);
        newIndex($$._false);
        $$.code = (lcode*) malloc(sizeof(lcode));
        init_code($$.code);
        codecat($$.code, $1.code);
        codecat($$.code, $3.code);
        char *arg =  (char*) malloc(3*sizeof(char));
        strcpy(arg, $1.dir);
        strcat(arg, " ");
        strcat(arg, $2);
        strcat(arg, " ");
        strcat(arg, $3.dir);
        insert_code($$.code, create_ci("if", arg, NULL, $$._true));
        insert_code($$.code, create_ci("goto", NULL, NULL, $$._false));
        //printf("B->E rel E\n");
        
    }

    ;
    
/* Or -> > | < | == | != | >= | <= */
op_rel : GT{strcpy($$, $1);}  
        | LT {strcpy($$, $1);}
        | EQ {strcpy($$, $1);}
        | DIF {strcpy($$, $1);}
        | GET {strcpy($$, $1);}
        | LET {strcpy($$, $1);};
    
%%

void yyerror(char* s){
    printf("%s , en  la linea %d\n", s, yylval.line);
}

void insert(char *id, int dir, int tipo, int sym, int args[], int num){    
    int i;
    strcpy(ctr_symbol.id, id);
    ctr_symbol.dir = dir;
    ctr_symbol.tipo = tipo;
    ctr_symbol.sym = sym;
    if(num != -1){
       for(i=0 ; i<num+1; i++){
            ctr_symbol.args[i] = args[i];
        }
    }
    ctr_symbol.num_args = num;
    //printf("el sp %d\n", symStack.sp);
    if(insert_symbol(ctr_symbol, symStack.sp)==0)
        yyerror("Id duplicado\n");
}


/*void = 0,  char = 1, int = 2, float = 3 y doble =4 */
int  max(int t1, int t2){
    if(t1 == t2)
        return t1;
    else if(t1==1 && t2 == 2){
        return t2;
    }else if( t1 == 2 && t2==1){
        return t1;
    }else if(t1==2 && t2 == 3){
        return t2;
    }else if( t1 == 3 && t2==2){
        return t1;
    }else if(t1==2 && t2 == 4){
        return t2;
    }else if( t1 == 4 && t2==2){
        return t1;
    }
    else if(t1==3 && t2 == 4){
        return t2;
    }else if( t1 == 4 && t2==3){
        return t1;
    }
    else{
        yyerror("Error Semántico: Los tipos no son compatibles funcion max");
        return -1;
    }
}


void amp(char *dir, int t1, int t2, char* res){
    if(t1 == t2){
        strcpy(res, dir);;
    }else if((t1==1 && t2==2) || (t1== 2 && t2 == 1)){
        char temp[20];
        newTemp(temp);
        //fprintf(yyout, "%s = (int) %s\n", temp, dir);
        strcpy(res, temp);
    }else if((t1==2 && t2==3) || (t1== 3 && t2 == 2)){
        char temp[20];
        newTemp(temp);
        //fprintf(yyout, "%s = (float) %s\n", temp, dir);
        strcpy(res, temp);
    }else if((t1==2 && t2==4) || (t1== 4 && t2 == 2)){
        char temp[20];
        newTemp(temp);
        //fprintf(yyout, "%s = (double) %s\n", temp, dir);
        strcpy(res, temp);
    }else if((t1==3 && t2==4) || (t1== 4 && t2 == 3)){
        char temp[20];
        newTemp(temp);
        //fprintf(yyout, "%s = (double) %s\n", temp, dir);
        strcpy(res, temp);
    }
    else{
        yyerror("Error Semántico: Los tipos no son compatibles funcion amp");
    }
}

void init(){
    init_symbol_table();
    create_symbol_table();
    create_type_table();
    init_index();
    gcode = (lcode *) malloc(sizeof(lcode));
    init_code(gcode);
    printf("Se creo gcode\n");
}


int main(int argc, char** argv){
    FILE* file;
    if(argc >1){
        file = fopen(argv[1], "r");
        if(file==NULL){
            printf("no existe el fichero %s\n", argv[1]);
            exit(1);
        }
        
        char nombre[50];
        strcpy(nombre, argv[1]);
        strcat(nombre, ".ci");
        yyout = fopen(nombre, "w");
        //printf("se abrio el archivo\n");
        yyin = file;        
        //yylval.sval = (char*) malloc(2*sizeof(char));
        init();
        yyparse();
        fclose(yyin);
        fclose(yyout);
    }
    
    return 1;
}

