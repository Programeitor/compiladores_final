 
%{
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "intermediate_code.h"
#include "y.tab.h"

%}

%option yylineno
%option noyywrap

id [a-zA-Z_][a-zA-Z0-9_]*
entero [0-9]+
flotante [0-9]+("."[0-9]+)?([Ee][+-]?[0-9]+)?[fF]
doble [0-9]+("."[0-9]+)?([Ee][+-]?[0-9]+)?

%%

void    {  
            yylval.line = yylineno;
            return VOID;
        }
        
char    {
            yylval.line = yylineno;
            return CHAR;
        }
        
int {
        //printf("int\n");
        yylval.line = yylineno;
        return INT;
    }

float   {
            //printf("float\n");
            yylval.line = yylineno;
            return FLOAT;
        }
        
double  {
            yylval.line = yylineno;
            return DOUBLE;
        }
        
struct  {
            yylval.line = yylineno;
            return STRUCT;
        }

define {
            yylval.line = yylineno;
            return DEFINE;
        }

for {
            yylval.line = yylineno;
            return FOR;
        }
do {
            yylval.line = yylineno;
            return DO;
        }

while {
            yylval.line = yylineno;
            return WHILE;
        }

if {
            yylval.line = yylineno;
            return IF;
        }

else {
            yylval.line = yylineno;
            return ELSE;
        }
        


{id}    { 
            //printf("id\n");            
            yylval.line = yylineno;            
            strcpy(yylval.sval, yytext);
            //printf("se hizo la copia \n");
            return ID;
        }

{entero}    {   
                //printf("entero\n");
                yylval.line = yylineno;
                yylval.num.tipo = 2;                
                strcpy(yylval.num.sval, yytext);
                yylval.num.ival = atoi(yytext);
                return NUM;
            }

{doble}  {   
                //printf("flotante\n");
                yylval.line = yylineno;
                yylval.num.tipo = 4;                
                strcpy(yylval.num.sval, yytext);
                yylval.num.dval = atof(yytext);
                return NUM;
            }
            
{flotante}  {   
                //printf("flotante\n");
                yylval.line = yylineno;
                yylval.num.tipo = 3;                
                strcpy(yylval.num.sval, yytext);
                yylval.num.dval = atof(yytext);
                return NUM;
            }
            
"+" {
        //printf("+\n");
        yylval.line = yylineno;
        return MAS;
    }
    
"*" {
        //printf("*\n");
        yylval.line = yylineno;
        return MUL;
    }

"-" {
        yylval.line = yylineno;
        return MENOS;
    }

"/" {
        yylval.line = yylineno;
        return DIV;
    }

"%" {
        yylval.line = yylineno;
        return MOD;
    }

"(" {
        //printf("(\n");
        yylval.line = yylineno;
        return LPAR;
    }
    
")" {
        //printf(")\n");
        yylval.line = yylineno;
        return RPAR;
    }
    
"=" {
        //printf("=\n");
        yylval.line = yylineno;
        return ASIG;
    }

"." {
        yylval.line = yylineno;
        return PUNTO;
    }

"," {
        //printf(",\n"); 
        yylval.line = yylineno;
        return COMA;
    }
    
";" {
        ///printf(";\n");
        yylval.line = yylineno;
        return PYC;
    }

"!" {
        yylval.line = yylineno;
        return NOT;
    }

"[" {
        yylval.line = yylineno;
        return LCOR;
    }

"]" {
        yylval.line = yylineno;
        return RCOR;
   }
   
"{" {
        yylval.line = yylineno;
        return LKEY;
    }

"}" {
        yylval.line = yylineno;
        return RKEY;
   }


">" {
        yylval.line = yylineno;
        strcpy(yylval.sval, yytext);
        return GT;
   }

"<" {
        yylval.line = yylineno;
        strcpy(yylval.sval, yytext);
        return LT;
   }

">=" {
        yylval.line = yylineno;
        strcpy(yylval.sval, yytext);
        return GET;
   }

"<=" {
        yylval.line = yylineno;
        strcpy(yylval.sval, yytext);
        return LET;
   }

"==" {
        yylval.line = yylineno;
        strcpy(yylval.sval, yytext);
        return EQ;
   }

"!=" {
        yylval.line = yylineno;
        strcpy(yylval.sval, yytext);
        return DIF;
   }

"||" {
        yylval.line = yylineno;        
        return OR;
    }

"&&" {
        yylval.line = yylineno;        
        return AND;
    }
[ \n\t]+ {}

. {printf("Error Léxico en la linea %d\n", yylineno);}

%%
    
