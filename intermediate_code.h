/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   intermediate_code.h
 * Author: ulises
 *
 * Created on 5 de junio de 2017, 19:59
 */

#ifndef INTERMEDIATE_CODE_H
#define INTERMEDIATE_CODE_H

#ifdef __cplusplus
extern "C" {
#endif
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
    
    extern FILE* yyout;
    
    struct _quadruple{
        char *op;
        char * arg1;
        char *arg2;
        char *res;
    };
    
    typedef struct _quadruple quadruple;
    
    typedef struct _inter_code icode;
    
    struct _inter_code{
        quadruple q;
        icode *next;
    };
    
    
    
    struct _list_code{
        icode *root;        
        int num;
    };
    
    typedef struct _list_code lcode;
    
    icode *create_ci(char *op, char *arg1, char *arg2, char *res);
    void insert_code(lcode *code, icode *_icode);
    void empty_code(lcode *code);    
    void delete_code(icode *code);
    void print_code(lcode *code);
    void print_ci(lcode *code);
    void init_code(lcode *code);
    void codecat(lcode *code1, lcode *code2);
    void asig(lcode *code, char* label1, char *label2);



#ifdef __cplusplus
}
#endif

#endif /* INTERMEDIATE_CODE_H */

