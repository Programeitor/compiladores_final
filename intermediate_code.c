/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
#include "intermediate_code.h"

icode *create_ci(char *op, char *arg1, char *arg2, char *res){
    //printf("creando nodo a insertar\n");
    icode *code = (icode*) malloc(sizeof(icode));
    quadruple q;
    //printf("cuadrupla\n");
    q.op = (char*) malloc(strlen(op)*sizeof(char));
    //printf("crear op\n");
    strcpy(q.op, op);
    //printf("copiar op\n");
    if(arg1 != NULL){
        q.arg1 = (char*) malloc(strlen(arg1)*sizeof(char));
        strcpy(q.arg1, arg1);
    }else{
        q.arg1 = NULL;
    }
    
    if(arg2 != NULL){
        q.arg2 = (char*) malloc(strlen(arg2)*sizeof(char));
        strcpy(q.arg2, arg2);
    }else{
        q.arg2 = NULL;
    }
    
    if(res != NULL){
        q.res = (char*) malloc(strlen(res)*sizeof(char));
        strcpy(q.res, res);
    }else{
        q.res = NULL;
    }
    
    code->q = q;
    code->next = NULL;    
    return code;
}


void insert_code(lcode *code, icode *_icode){
    
    if(code->root==NULL){
        //printf("insertando en root nulo \n");
        code->root = _icode;
        //code->end = _icode;
        //printf("%s, %s, %s, %s\n", code->root->q.op, code->root->q.arg1, code->root->q.arg2, code->root->q.res);
        //print_ci(code);
    }else{        
        icode *tmp = code->root;
        while(tmp->next != NULL){            
            tmp = tmp->next;
        }
        tmp->next = _icode;  
    }       
    code->num++;
}

void codecat(lcode *code1, lcode *code2){
    //printf("concatenando\n");
    icode *tmp = code1->root;
    if(code2->root==NULL){
        //printf("code2 nulo\n");
        return;
    }
    /*if(!tmp && !code2->root){  
        printf("fin 1concatenando\n");
        return;
    }*/
    if(!tmp){
        code1->root = code2->root;
        code1->num = code2->num;
        //printf("codigo 1 nulo\n");
        return;
    }
    while(tmp->next!=NULL){
        tmp = tmp->next;
    }
    tmp->next = code2->root;
    code1->num += code2->num;
    //printf("fin 3concatenando\n");
}

void print_ci(lcode *code){
    icode *tmp = code->root;
    if(!tmp) {
        printf("no hay codigo para imprimir\n");
        return;
    }
    
    do{
        printf("%s, %s, %s, %s\n", tmp->q.op, tmp->q.arg1, tmp->q.arg2, tmp->q.res);
        tmp = tmp->next;
    }while(tmp!=NULL);
}

void print_code(lcode *code){
    printf("impimiendo en archivo\n");
    icode *tmp = code->root;
    if(!tmp){
        printf("No se genero código\n");
        return;
    }
    
    do{
        if(strcmp(tmp->q.op, "label")==0){
            fprintf(yyout,"%s: ",tmp->q.res);
        }else if(strcmp(tmp->q.op, "return")==0){
            fprintf(yyout, "return %s\n", tmp->q.res);
        }else if(strcmp(tmp->q.op, "=")==0){
            fprintf(yyout, "%s = %s\n", tmp->q.res, tmp->q.arg1);
        }else if(strcmp(tmp->q.op, "if")==0){
            fprintf(yyout, "if %s goto %s\n", tmp->q.arg1, tmp->q.res);
        }else if(strcmp(tmp->q.op, "goto")==0){
            fprintf(yyout, "goto %s\n", tmp->q.res);
        }else{
            fprintf(yyout, "%s = %s %s %s", tmp->q.res, tmp->q.arg1, tmp->q.op,
                    tmp->q.arg2);
        }
        tmp = tmp->next;
    }while(tmp!=NULL);
    printf("terminando de escribir en archivo");
}

void empty_code(lcode *code){
    delete_code(code->root);
    code->num=0;
}

void delete_code(icode *code){
    if(code->next!= NULL){
        delete_code(code->next);
    }
    free(code);
}

void init_code(lcode *code){
    //printf("iniciando code\n");        
    code->num=0;    
    code->root = NULL;  
    //code->end = NULL;
    //printf("terminando init code\n");
}


void asig(lcode *code, char* label1, char *label2){
    icode *tmp = code->root;
    while(tmp->next!=NULL){
        if(strcmp(tmp->q.res, label1)==0){
            strcpy(tmp->q.res,label2);
        }
        tmp = tmp->next;
    }
}

